1. List the books Authored by Marjorie Green - 213-46-8915
- BU1032: The Busy Executive's Database Guide
- BU2075: You Can Combat Computer Stress!

2. List the books Authored by Michael O'Leary. - 267-41-2394
- BU1111: Cooking with Computers

3. Write the author/s of "The Busy Executive's Database Guide".
- Marjorie Green
- Abraham Bennet

4. Identify the publisher of "But Is It User Friendly?".
- Algodata Infosystems

5. List the books published by Algodata Infosystems.
- The Busy Executive's Database Guide
- Cooking with Computers
- Straight Talk About Computers
- But Is It User Friendly ?
- Secrets of Silicon Valley
- Net etiquette